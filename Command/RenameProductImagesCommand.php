<?php

namespace Nitra\ProductBundle\Command;

use Nitra\ExtensionsBundle\Command\NitraContainerAwareCommand as ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;

class RenameProductImagesCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('nitra:update:product:images')
            ->setDescription('Generate unique image name to products');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $dm          = $this->getDocumentManager();
        // ТОвар
        $products    = $dm->getRepository('NitraProductBundle:Product')->findAll();
        // строка изображений которые не найденны в каталоге
        $imgNotFound = array();
        // путь к web
        $rootDir     = $this->getContainer()->get('kernel')->getRootDir() . '/../web';
        // прогресс бар
        $progress    = $this->getProgressHelper();
        $progress->start($output, count($products));
        $fs          = new Filesystem();

        $i = 0;
        foreach ($products as $pr) {
            // переименование изображения
            $reImage = false;
            // Для изображений 
            if ($pr->getImage() && strpos($pr->getImage(), '_')) {
                // пуьт к оригинальному файлу
                $originFile = $rootDir . $pr->getImage();
                // если существует оригинальный файл
                if (file_exists($originFile)) {

                    $reImage           = true;
                    $pathParts         = pathinfo($originFile);
                    // устанавливаем уникальное имя
                    $newFileName       = $this->getUnicFileName($pathParts['dirname'], $pathParts['extension']) . '.' . $pathParts['extension'];
                    $targetFile        = $pathParts['dirname'] . '/' . $newFileName;
                    $productImageParts = pathinfo($pr->getImage());
                    // переименовываем файл
                    $fs->rename($originFile, $targetFile);
                    $pr->setImage($productImageParts['dirname'] . '/' . $newFileName);
                } else {
                    $imgNotFound[] = array(
                        $pr->getId(),
                        $pr->getImage());
                }
            }
            // если есть дополнительные изображения
            if ($pr->getImages()) {
                // обьявляем перкеменные
                $reImages = false;
                $images   = array();

                foreach ($pr->getImages() as $image) {
                    // путьк оригинльному файлу
                    $originFile = $rootDir . $image;

                    if (file_exists($originFile)) {
                        // если длинное имя файла
                        if (strpos($image, '_')) {
                            // название изменялось
                            $reImages          = true;
                            $pathParts         = pathinfo($originFile);
                            // устанавливаем новое имя
                            $newFileName       = $this->getUnicFileName($pathParts['dirname'], $pathParts['extension']) . '.' . $pathParts['extension'];
                            $targetFile        = $pathParts['dirname'] . '/' . $newFileName;
                            $productImageParts = pathinfo($image);
                            // переименовываем файл
                            $fs->rename($originFile, $targetFile);
                            $images[]          = $productImageParts['dirname'] . '/' . $newFileName;
                        } else {
                            $images[] = $image;
                        }
                    } else {
                        $images[]      = $image;
                        $imgNotFound[] = array(
                            $pr->getId(),
                            $image);
                    }
                }
                // если изменлось хоть одно дополнительное изображение
                if ($reImages) {
                    $pr->setImages($images);
                }

                // если изменялось изображение или дополнительное изображение
                if ($reImages || $reImage) {
                    $i++;
                }

                if ($i == 200) {
                    $dm->flush();
                    $dm->getRepository('NitraProductBundle:Product')->clear();
                    $i = 0;
                }
            }
            $progress->advance();
        }

        $dm->flush();
        $progress->finish();

        if (count($imgNotFound) > $output->getVerbosity()) {
            $table = $this->getTableHelper();
            $table->setHeaders(array(
                'product',
                'image'));
            $table->setRows($imgNotFound);
            $table->render($output);
        }
    }

    /**
     * Функция для создания нового уникально имени
     * @param type $dir путь к файлу
     * @param type $extension - расширение файла
     */
    protected function getUnicFileName($dir, $extension, $i = 0)
    {
        $uniq = uniqid();
        if (file_exists($dir . '/' . $uniq . '.' . $extension)) {
            $i++;
            // предотвращаем зацыкливание
            if ($i == 20) {
                throw new \Exception('Ошибка создания нового имени для файла');
            }
            return $this->getUnicFileName($dir, $extension, $i);
        }

        return $uniq;
    }
}