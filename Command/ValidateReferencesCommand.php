<?php

namespace Nitra\ProductBundle\Command;

use Nitra\ExtensionsBundle\Command\NitraContainerAwareCommand;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Helper\ProgressHelper;

class ValidateReferencesCommand extends NitraContainerAwareCommand
{
    /**
     * @var \MongoDB
     */
    protected $mongodb;

    /**
     * @var InputInterface
     */
    protected $input;

    /**
     * @var OutputInterface
     */
    protected $output;

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('nitra:validate-references');
    }

    /**
     * {@inheritdoc}
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        // get default database name
        $dbName = $this->getDocumentManager()
            ->getConfiguration()
            ->getDefaultDB();

        // get mongodb instance
        $this->mongodb = $this->getDocumentManager()
            ->getConnection()
            ->selectDatabase($dbName);

        // set input instance to $this
        $this->input  = $input;
        // set output instance to $this
        $this->output = $output;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // get all metadata
        $metadata = $this->getAllClassMetadata();

        // get references
        $references = $this->collectReferences($metadata);

        // iterate all references
        foreach ($references as $collection => $collectionReferences) {
            $output->writeln(sprintf(
                'Processing "%s" collection:',
                $this->format($collection, self::STYLE_BOLD, self::COLOR_RED)
            ));
            if (!$this->processValidateCollection($collection, $collectionReferences)) {
                $output->writeln("\tAll references are correct");
            }
        }
    }

    /**
     * Get all doctrine class metadata
     *
     * @return \Doctrine\ODM\MongoDB\Mapping\ClassMetadata[]
     */
    protected function getAllClassMetadata()
    {
        // get all class metadata from document manager
        $metadata = $this->getDocumentManager()
            ->getMetadataFactory()
            ->getAllMetadata();

        // define multisort array
        $multisort = array();
        // fill multisort
        foreach ($metadata as $key => $cm) {
            $multisort[$key] = $cm->rootDocumentName;
        }

        // sort class metadata
        array_multisort($multisort, SORT_NATURAL, $metadata);

        return $metadata;
    }

    /**
     * Collect references from metadata
     *
     * @param \Doctrine\ODM\MongoDB\Mapping\ClassMetadata[] $metadata
     *
     * @return array
     */
    protected function collectReferences($metadata)
    {
        // define result references array
        $references = array();

        // iterate metadata
        foreach ($metadata as $cm) {
            // define array for collect references for collection
            $collectionReferences = array();
            // iterate association names
            foreach ($cm->getAssociationNames() as $associationName) {
                // get mapping for field
                $fieldMapping = $cm->getFieldMapping($associationName);
                // if key 'references' exists in field mapping and him equal true
                if (array_key_exists('reference', $fieldMapping) && $fieldMapping['reference']) {
                    // add field to collection references with type (many or one)
                    $collectionReferences[$associationName] = $fieldMapping['type'];
                }
            }
            // if collection references is not empty
            if (!empty($collectionReferences)) {
                // add collection references to result
                // if key exists in result
                $references[$cm->collection] = isset($references[$cm->collection])
                    // merge arrays
                    ? array_merge($references[$cm->collection], $collectionReferences)
                    // set array
                    : $collectionReferences;
            }
        }

        // return all references
        return $references;
    }

    /**
     * Process validate collection documents
     *
     * @param string    $collectionName     Name of mongodb collection
     * @param array     $references         List of references (key - reference field name, value - type of reference)
     *
     * @return boolean
     */
    protected function processValidateCollection($collectionName, $references)
    {
        // get mongodb collection
        $collection = $this->mongodb->selectCollection($collectionName);

        // find all documents
        $documents  = $collection->find(array(), array_keys($references));
        // if documents not found
        if ($documents->count() == 0) {
            return false;
        }

        // get progress helper
        $progress   = $this->getProgressHelper();
        // set format to progress helper
        $progress->setFormat(ProgressHelper::FORMAT_NORMAL);
        // start progress
        $progress->start($this->output, $documents->count());

        // define invalids array
        $invalids   = array();
        // iterate documents
        foreach ($documents as $document) {
            // iterate references
            foreach ($references as $field => $type) {
                // if reference not exists
                if (!array_key_exists($field, $document) || !$document[$field]) {
                    // skip document
                    continue;
                }

                // add found invalids to all invalids
                $invalids = array_merge(
                    $invalids,
                    $this->findInvalidReferences($type, $field, $document)
                );
            }
            // advance progress bar
            $progress->advance();
        }
        // finish progress
        $progress->finish();

        // print all invalids
        $this->printInvalidReferences($invalids, $collectionName);

        // return true, if invalids not found
        return (bool) $invalids;
    }

    /**
     * Find invalid references for document
     *
     * @param string $type
     * @param string $field
     * @param array  $document
     *
     * @return array
     */
    protected function findInvalidReferences($type, $field, $document)
    {
        // define invalids array
        $invalids = array();
        // get reference documents as array
        $referenceDocuments = ($type == 'many')
            ? $document[$field]
            : array($document[$field]);

        // iterate references
        foreach ($referenceDocuments as $referenceDocument) {
            // find reference document
            $ref = $this->mongodb->selectCollection($referenceDocument['$ref'])->findOne(array(
                '_id' => $referenceDocument['$id'],
            ));

            // if reference not found
            if (!$ref) {
                // add to invalids
                $invalids[(string) $referenceDocument['$id']] = array(
                    $document['_id'],
                    $referenceDocument['$ref'],
                );
            }
        }

        return $invalids;
    }

    /**
     * Print invalid references
     *
     * @param array     $invalids       Array of invalid references
     * @param string    $collection     Collection name
     */
    protected function printInvalidReferences($invalids, $collection)
    {
        // iterate invalids
        foreach ($invalids as $id => $invalid) {
            // get document id and reference collection from item
            list ($documentId, $ref) = $invalid;

            // write invalid message
            $this->output->writeln(sprintf(
                '%sInvalid reference on "%s" collection on document with id "%s" to "%s" collection by id "%s"',
                "\t",
                $collection,
                $this->format($documentId, self::STYLE_NONE, self::COLOR_RED),
                $ref,
                $this->format($id, self::STYLE_NONE, self::COLOR_RED)
            ));
        }
    }
}