<?php

namespace Nitra\ProductBundle\Command;

use Nitra\ExtensionsBundle\Command\NitraContainerAwareCommand;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;

class GenerateFullSearchNamesCommand extends NitraContainerAwareCommand
{
    /**
     * @var \MongoDB
     */
    protected $mongodb;

    /**
     * @var InputInterface
     */
    protected $input;

    /**
     * @var OutputInterface
     */
    protected $output;

    /**
     * @var string Collection name of products
     */
    protected $productCollectionName;

    /**
     * @var array configuration for generate full search name
     */
    protected $config;

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this->setName('nitra:generate-full-search-names')
            ->addOption('clear', 'c', InputOption::VALUE_NONE, 'Clear full names before generate');
    }

    /**
     * @inheritdoc
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        // get default database name
        $dbName = $this->getDocumentManager()
            ->getConfiguration()
            ->getDefaultDB();

        // get mongodb instance
        $this->mongodb = $this->getDocumentManager()
            ->getConnection()
            ->selectDatabase($dbName);

        // save product collection name
        $cm = $this->getDocumentManager()
            ->getRepository('NitraProductBundle:Product')
            ->getClassMetadata();
        $this->productCollectionName = $cm->getCollection();

        // set input instance to $this
        $this->input  = $input;
        // set output instance to $this
        $this->output = $output;

        $this->config = $this->getContainer()->get('nitra.search.listener')->getConfiguration();
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $collection = $this->mongodb->selectCollection($this->productCollectionName);

        if ($input->getOption('clear')) {
            $this->clearFullNames($collection);
        }

        $products = $this->getProducts($collection);
        $progress = $this->getProgressHelper();
        $progress->start($output, $products->count());

        foreach ($products as $product) {
            $collection->update(array(
                '_id' => $product['_id'],
            ), array(
                '$set' => array(
                    'fullNameForSearch' => $this->generateFullName($product),
                ),
            ));
            $progress->advance();
        }
        $progress->finish();
    }

    /**
     * Clear all full search names for products
     *
     * @param \MongoCollection $collection
     */
    protected function clearFullNames($collection)
    {
        $collection->update(array(), array(
            '$unset' => array(
                'fullNameForSearch' => 1,
            ),
        ), array(
            'multiple' => true,
        ));
    }

    /**
     * Get products without fullNameForSearch field or with empty him
     *
     * @param \MongoCollection $collection
     *
     * @return \MongoCursor
     */
    protected function getProducts($collection)
    {
        return $collection->find(array(
            '$or' => array(
                array(
                    'fullNameForSearch' => array(
                        '$exists' => false,
                    ),
                ),
                array(
                    'fullNameForSearch' => '',
                ),
            ),
        ));
    }

    /**
     * Generate full name by configuration
     *
     * @param array $product
     *
     * @return string
     */
    protected function generateFullName($product)
    {
        $words = array();
        foreach ($this->config['fields'] as $field) {
            if (is_string($field)) {
                $getter = lcfirst(preg_replace('/^get/', '', $field));
                if (isset($product[$getter])) {
                    $words[] = $product[$getter];
                }
            } elseif (is_array($field)) {
                $word = $product;
                foreach ($field as $subField) {
                    $getter = lcfirst(preg_replace('/^get/', '', $subField));
                    if (!isset($word[$getter])) {
                        break;
                    }

                    $word = $word[$getter];

                    if (isset($word['$id'])) {
                        $word = $this->mongodb->selectCollection($word['$ref'])->findOne(array(
                            '_id' => $word['$id'],
                        ));
                    }
                }
                if (is_string($word)) {
                    $words[] = $word;
                }
            }
        }

        return implode(' ', $words);
    }
}