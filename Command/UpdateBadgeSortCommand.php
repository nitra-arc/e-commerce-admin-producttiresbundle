<?php

namespace Nitra\ProductBundle\Command;

use Nitra\ExtensionsBundle\Command\NitraContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateBadgeSortCommand extends NitraContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('nitra:update:badge:sorts')
            ->setDescription('Generate badgeSorts field to all products');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $js = 'function () {
            var ret = 0;
            db.Product.find().forEach(function(p) {
                var badgeSorts = {};
                var badgeSortOrder = -1;
                if (p.hasOwnProperty("badge") && p.badge && p.badge.hasOwnProperty("$id")) {
                    var badge = db.Badge.findOne({_id: p.badge["$id"]});
                    if (badge.hasOwnProperty("sortOrder")) {
                        badgeSortOrder = badge.sortOrder;
                    } else {
                        badgeSortOrder = 0;
                    }
                    if (badge.hasOwnProperty("identifier")) {
                        badgeSorts[badge.identifier] = badgeSortOrder;
                    }
                }
                badgeSorts["all"] = badgeSortOrder;
                db.Product.update({_id: p._id}, {
                    $set: {
                        "badgeSorts": badgeSorts
                    }
                });
                ret ++;
            });
            return ret;
        }';
        $exec = $this->getMongoDB()->command(array('eval' => $js, 'nolock' => true));
        if ($exec['ok']) {
            $output->writeln('Updateted ' . ($exec['retval']) . ' products');
        } else {
            $output->writeln('Saving failed with message: "' . $exec['errmsg'] . '"');
        }
    }

    protected function getMongoDB()
    {
        $dm = $this->getDocumentManager();
        return $dm->getConnection()->getMongo()->selectDB($dm->getConfiguration()->getDefaultDB());
    }
}