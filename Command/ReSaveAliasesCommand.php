<?php

namespace Nitra\ProductBundle\Command;

use Nitra\ExtensionsBundle\Command\NitraContainerAwareCommand;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Helper\ProgressHelper;

class ReSaveAliasesCommand extends NitraContainerAwareCommand
{
    /**
     * @var \MongoDB
     */
    protected $mongodb;

    /**
     * @var InputInterface
     */
    protected $input;

    /**
     * @var OutputInterface
     */
    protected $output;

    /**
     * @var null|string Doctrine document for re-save aliases
     */
    protected $document = null;

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('nitra:re-save-aliases')
            ->addOption('document', 'd', InputOption::VALUE_OPTIONAL, 'Document for re-save, as example - NitraProductBundle:Product');
    }

    /**
     * {@inheritdoc}
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        // get default database name
        $dbName = $this->getDocumentManager()
            ->getConfiguration()
            ->getDefaultDB();

        // get mongodb instance
        $this->mongodb = $this->getDocumentManager()
            ->getConnection()
            ->selectDatabase($dbName);

        // set input instance to $this
        $this->input  = $input;
        // set output instance to $this
        $this->output = $output;

        // if input option document is specified
        if ($input->getOption('document')) {
            // set value to protected variable
            $this->document = $input->getOption('document');
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // get all metadata
        $metadata = $this->getAllClassMetadata();

        // define processed classes
        // for break multiple re save one collection
        $processedCollections = array();
        // iterate metadata
        foreach ($metadata as $cm) {
            // skip if field aliasEn not exists
            if (!$cm->hasField('aliasEn')) {
                continue;
            }

            // skip if collection processed
            if (in_array($cm->collection, $processedCollections)) {
                continue;
            }

            // write to output
            $message = sprintf(
                'Processing "%s" ("%s"):',
                $this->format($cm->rootDocumentName, self::STYLE_BOLD, self::COLOR_RED),
                $this->format($cm->collection, self::STYLE_BOLD)
            );
            $output->writeln($message);

            // process re saving
            $this->processReSave($cm);
            // add collection to processed collections
            $processedCollections[] = $cm->collection;
        }
    }

    /**
     * Get all doctrine class metadata
     *
     * @return \Doctrine\ODM\MongoDB\Mapping\ClassMetadata[]
     */
    protected function getAllClassMetadata()
    {
        // if is defined document (input option)
        if ($this->document) {
            // return only one class metadata
            // for document from input options
            return array(
                $this->getDocumentManager()
                    ->getRepository($this->document)
                    ->getClassMetadata(),
            );
        }

        // get all class metadata from document manager
        $metadata = $this->getDocumentManager()
            ->getMetadataFactory()
            ->getAllMetadata();

        // define multisort array
        $multisort = array();
        // fill multisort
        foreach ($metadata as $key => $cm) {
            $multisort[$key] = $cm->rootDocumentName;
        }

        // sort class metadata
        array_multisort($multisort, SORT_NATURAL, $metadata);

        return $metadata;
    }

    /**
     * Process re save documents
     *
     * @param \Doctrine\ODM\MongoDB\Mapping\ClassMetadata $cm
     */
    protected function processReSave($cm)
    {
        // get mongodb collection
        $collection = $this->mongodb->selectCollection($cm->collection);

        // find documents
        $documents  = $this->getDocuments($cm, $collection);

        // if documents not found
        if (!$documents->count()) {
            $this->output->writeln("\tNot found\n");
            return;
        }
        $this->processDocuments($documents, $cm, $collection);
    }

    /**
     * Get documents
     *
     * @param \Doctrine\ODM\MongoDB\Mapping\ClassMetadata   $cm
     * @param \MongoCollection                              $collection
     *
     * @return \MongoCursor
     */
    protected function getDocuments($cm, $collection)
    {
        // find documents
        $documents  = $collection->find();
        if ($cm->collection == 'Categories') {
            // if collection is category
            // add sort by level
            $documents->sort(array(
                'level'     => 1,
            ));
        }

        return $documents;
    }

    /**
     * Process regenerate documents
     *
     * @param \MongoCursor                                  $documents
     * @param \Doctrine\ODM\MongoDB\Mapping\ClassMetadata   $cm
     * @param \MongoCollection                              $collection
     */
    protected function processDocuments($documents, $cm, $collection)
    {
        $updated = 0;

        // define exists aliases array by locale
        $aliases = array(
            'Ru' => array(),
            'En' => array(),
        );

        // get progress helper
        $progress = $this->getProgressHelper();
        // set progress format
        $progress->setFormat(ProgressHelper::FORMAT_NORMAL);
        // start progress
        $progress->start($this->output, $documents->count());
        foreach ($documents as $item) {
            // get document name
            $name = array_key_exists('name', $item)
                ? $item['name']
                : '';

            // generate unique aliases
            $sets = $this->generateAliases($name, $aliases);
            // add additions fields (fullUrlAlias...)
            $this->additions($cm->rootDocumentName, $item, $sets);

            // advance progress bar
            $progress->advance();

            if (!$this->checkNeedUpdate($sets, $item)) {
                continue;
            }
            // update document
            $collection->update(array(
                '_id' => $item['_id'],
            ), array(
                '$set' => $sets,
            ));
            $updated ++;
        }

        // finish progress bar
        $progress->finish();

        if ($this->output->getVerbosity() >= OutputInterface::VERBOSITY_VERBOSE) {
            $this->output->writeln(sprintf(
                '%sUpdated %s/%s documents%s',
                "\t",
                $this->format($updated, self::STYLE_BOLD, self::COLOR_RED),
                $this->format($documents->count(), self::STYLE_BOLD),
                "\n"
            ));
        }
    }

    /**
     * Add additions values to sets by class
     *
     * @param string $class Class name
     * @param array  $item  Document
     * @param array  $sets  Aliases
     */
    protected function additions($class, $item, &$sets)
    {
        // switch by class
        switch ($class) {
            // if category
            case 'Nitra\ProductBundle\Document\Category':
                // get parent category id
                $parentId = isset($item['parent']['$id'])
                    ? $item['parent']['$id']
                    : null;
                // merge sets
                $sets = array_merge(
                    $sets,
                    $this->generateFullUrlAliases($parentId, $sets)
                );
                break;
            // if product
            case 'Nitra\ProductBundle\Document\Product':
                // get category id from model of product
                $model = $this->getDocumentManager()
                    ->createQueryBuilder('NitraProductBundle:Model')
                    ->hydrate(false)->select('category', 'aliasEn', 'aliasRu')
                    ->field('id')->equals($item['model']['$id'])
                    ->getQuery()->getSingleResult();

                // iterate sets
                $fullUrlAliasesSets = $sets;
                foreach ($fullUrlAliasesSets as $aliasLocaled => $value) {
                    $modelAlias = 
                        $model != null ?
                            array_key_exists($aliasLocaled, $model)
                                ? $model[$aliasLocaled]
                                : ''
                        : '';
                        
                    $fullUrlAliasesSets[$aliasLocaled] = $modelAlias . ($modelAlias ? '/' : '') . $fullUrlAliasesSets[$aliasLocaled];
                }
                // merge sets
                $sets = array_merge(
                    $sets,
                    $this->generateFullUrlAliases($model['category']['$id'], $fullUrlAliasesSets)
                );
                break;
        }
    }

    /**
     * Generate full url aliases for category or product
     *
     * @param string $id        Category id
     * @param array  $aliases   Array of generated aliases
     *
     * @return array Aliases
     */
    protected function generateFullUrlAliases($id, $aliases)
    {
        // get category collection name
        $collectionName = $this->getDocumentManager()->getRepository('NitraProductBundle:Category')
            ->getClassMetadata()->collection;

        // get mongodb collection
        $collection = $this->mongodb->selectCollection($collectionName);

        // find category
        $parent = $collection->findOne(array(
            '_id'            => $id,
        ), array(
            'fullUrlAliasEn' => 1,
            'fullUrlAliasRu' => 1,
        ));
        // if not id or not category
        if (!$id || !$parent) {
            // return array with url aliases as simple aliases
            return array(
                'fullUrlAliasEn' => $aliases['aliasEn'],
                'fullUrlAliasRu' => $aliases['aliasRu'],
            );
        }

        // define full url aliases array
        $fullUrlAliases = array(
            'fullUrlAliasEn' => array_key_exists('fullUrlAliasEn', $parent)
                ? $parent['fullUrlAliasEn']
                : null,
            'fullUrlAliasRu' => array_key_exists('fullUrlAliasRu', $parent)
                ? $parent['fullUrlAliasRu']
                : null,
        );

        // add simple aliases to result
        $fullUrlAliases['fullUrlAliasEn'] .= ('/' . $aliases['aliasEn']);
        $fullUrlAliases['fullUrlAliasRu'] .= ('/' . $aliases['aliasRu']);

        // return array with changes
        return $fullUrlAliases;
    }

    /**
     * Generate aliases
     *
     * @param string            $name       Source name
     * @param array             $aliases    Array with exists aliases
     *
     * @return array List of aliases
     */
    protected function generateAliases($name, &$aliases)
    {
        // transliterate name to english
        $aliasEn = $this->transliterate('English', $name);
        // transliterate name to russian
        $aliasRu = $this->transliterate('Russian', $name);

        // return array with unique aliases
        return array(
            'aliasEn' => $this->uniqueAlias($aliases, 'En', $aliasEn),
            'aliasRu' => $this->uniqueAlias($aliases, 'Ru', $aliasRu),
        );
    }

    /**
     * Get unique alias
     *
     * @param array     $aliases    List of exists aliases
     * @param string    $locale     Locale
     * @param string    $alias      Generated alias
     *
     * @return string   Unique alias
     */
    protected function uniqueAlias(&$aliases, $locale, $alias)
    {
        // if alias has already been
        if (isset($aliases[$locale][$alias])) {
            // generate new alias
            $newAlias = $alias . '-' . $aliases[$locale][$alias];
            // increment exists alias
            $aliases[$locale][$alias] ++;
            // add new alias
            $aliases[$locale][$newAlias] = 1;

            // return new alias
            return $newAlias;
        } else {
            // add alias to exists
            $aliases[$locale][$alias] = 1;

            // return alias
            return $alias;
        }
    }

    /**
     * Transliterate name
     *
     * @param string $class Name of class for transliterate
     * @param string $name  Source name for generate alias
     *
     * @return string   Generated alias
     */
    protected function transliterate($class, $name)
    {
        // define callable string
        $callable = '\Nitra\ProductBundle\Sluggable\Transliterators\\' .
            $class .
            '::transliterate';

        // return result of call transliterate with name
        return call_user_func($callable, $name);
    }

    /**
     * Check of need or no to update document
     *
     * @param array $changes
     * @param array $item
     *
     * @return boolean
     */
    protected function checkNeedUpdate($changes, $item)
    {
        // define need to update as false
        $need = false;
        // iterate changes
        foreach ($changes as $key => $value) {
            // if item not has field
            if (!array_key_exists($key, $item)) {
                // set need to update as true
                $need = true;
                continue;
            }

            // if item value not equal generated value
            if ($item[$key] != $value) {
                // set need to update as true
                $need = true;
            }
        }

        return $need;
    }
}