<?php

namespace Nitra\ProductBundle\Command;

use Nitra\ExtensionsBundle\Command\NitraContainerAwareCommand;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputInterface;

class GenerateNamesCommand extends NitraContainerAwareCommand
{
    /**
     * @var \MongoDB
     */
    protected $mongodb;

    /**
     * @var InputInterface
     */
    protected $input;

    /**
     * @var OutputInterface
     */
    protected $output;

    /**
     * @var string
     */
    protected $storeId;

    /**
     * @var array Default settings prototype
     */
    protected $settingsPrototype = array(
        'modelFull'    => array(),
        'modelShort'   => array(),
        'productFull'  => array(),
        'productShort' => array(),
    );

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this->setName('nitra:generate-names');
    }

    /**
     * @inheritdoc
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        // get default database name
        $dbName = $this->getDocumentManager()
            ->getConfiguration()
            ->getDefaultDB();

        // get mongodb instance
        $this->mongodb = $this->getDocumentManager()
            ->getConnection()
            ->selectDatabase($dbName);

        // set input instance to $this
        $this->input  = $input;
        // set output instance to $this
        $this->output = $output;
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // write message to console
        $output->writeln('Processing products:');
        // find all products
        list($products, $collection) = $this->find('NitraProductBundle:Product');
        // call process generate names
        $this->processGenerate($products, $collection);

        // write message to console
        $output->writeln('Processing models:');
        // find all models
        list($models, $collection)   = $this->find('NitraProductBundle:Model');
        // call process generate names
        $this->processGenerate($models, $collection);
    }

    /**
     * Get store id
     *
     * @return string
     */
    protected function getStoreId()
    {
        // if this has store id
        if ($this->storeId) {
            return $this->storeId;
        }

        // find first store
        $store = $this->mongodb->selectCollection('Stores')->findOne();

        // save store id to this
        $this->storeId = $store['_id'];

        // return store id
        return $this->storeId;
    }

    /**
     * Find all documents
     *
     * @param string $document Document name
     *
     * @return array
     */
    protected function find($document)
    {
        // get collection name
        $collectionName = $this->getDocumentManager()
            ->getRepository($document)
            ->getClassMetadata()
            ->getCollection();

        // select mongo collection
        $collection = $this->mongodb->selectCollection($collectionName);

        // return array
        return array(
            // cursor
            $collection->find(),
            // collection
            $collection,
        );
    }

    /**
     * Process generate for collection
     *
     * @param \MongoCursor      $documents
     * @param \MongoCollection  $collection
     */
    protected function processGenerate($documents, $collection)
    {
        // get progress helper
        $progress = $this->getProgressHelper();
        // start progress bar
        $progress->start($this->output, $documents->count());

        // iterate documents
        foreach ($documents as $document) {
            // define key for get settings, if category exists, then is model
            $key = isset($document['category'])
                ? 'model'
                : 'product';
            // get settings
            $settings = $this->getSettings($document);

            // update document
            $collection->update(array(
                '_id' => $document['_id'],
            ), array(
                '$set' => array(
                    // generate names
                    'fullNames.' . $this->getStoreId()  => $this->generateName($settings[$key . 'Full'], $document),
                    'shortNames.' . $this->getStoreId() => $this->generateName($settings[$key . 'Short'], $document),
                ),
            ));

            // advance progress bar
            $progress->advance();
        }

        // finish progress bar
        $progress->finish();
    }

    /**
     * Get settings for generate name
     *
     * @param array $document
     *
     * @return array
     */
    protected function getSettings($document)
    {
        // get id of category from model or product
        if (array_key_exists('model', $document)) {
            $model = $this->mongodb->selectCollection($document['model']['$ref'])
                ->findOne(array(
                    '_id' => $document['model']['$id'],
                ));
            $categoryId = $model['category']['$id'];
        } else {
            $categoryId = $document['category']['$id'];
        }

        // recursive get category settings (and store, if for categories tree settings not specified)
        $settings = $this->getSettingsFromCategory($categoryId);

        // iterate settings
        foreach ($settings as &$itemSettings) {
            // and sort him by order field
            usort($itemSettings, function($a, $b) {
                if ($a['order'] == $b['order']) {
                    return 0;
                }

                return ($a['order'] > $b['order']) ? 1 : -1;
            });
        }

        // return settings
        return $settings;
    }

    /**
     * Get settings from category tree
     *
     * @param \MongoId $categoryId
     * @param null|array $settings
     *
     * @return array
     */
    protected function getSettingsFromCategory($categoryId, $settings = null)
    {
        // if settings is null
        if ($settings === null) {
            // get default settings prototype
            $settings = $this->settingsPrototype;
        }

        // get collection name for categories
        $collectionName = $this->getDocumentManager()
            ->getRepository('NitraProductBundle:Category')
            ->getClassMetadata()
            ->getCollection();

        // find category by id
        $category = $this->mongodb->selectCollection($collectionName)
            ->findOne(array(
                '_id' => $categoryId,
            ));

        // merge category settings with exists settings
        $settings = $this->mergeSettings($settings, $category);

        // check of all settings is filled or no
        $allSettingsIsFilled = true;
        foreach ($settings as $itemNameSettings) {
            if (empty($itemNameSettings)) {
                $allSettingsIsFilled = false;
            }
        }

        // if all settings is filled
        if ($allSettingsIsFilled) {
            // return him
            return $settings;
        // if category not has parent and settings is not fully filled
        } elseif (!$category['parent']) {
            // return merged settings with settings from store
            return $this->addStoreSettings($settings);
        // else if settings not fully filled and category has parent
        } else {
            // return merged settings with settings from parent category
            return $this->getSettingsFromCategory($category['parent']['$id'], $settings);
        }
    }

    /**
     * Add store settings for generate name
     *
     * @param array $settings
     *
     * @return array
     */
    protected function addStoreSettings($settings)
    {
        // get first store
        $store = $this->mongodb->selectCollection('Stores')->findOne();

        // get store settings
        $storeSettings = $store['productManagement'];

        // return merged settings array with store settings
        return $this->mergeSettings($settings, $storeSettings);
    }

    /**
     * Merge settings
     *
     * @param array $settings
     * @param array $document
     *
     * @return array
     */
    protected function mergeSettings($settings, $document)
    {
        // if settings by key is empty and document settings is not empty
        if (empty($settings['modelFull']) && !empty($document['fullModelName'])) {
            // add to settings
            $settings['modelFull'] = $document['fullModelName'];
        }
        if (empty($settings['modelShort']) && !empty($document['shortModelName'])) {
            $settings['modelShort'] = $document['shortModelName'];
        }
        if (empty($settings['productFull']) && !empty($document['fullProductName'])) {
            $settings['productFull'] = $document['fullProductName'];
        }
        if (empty($settings['productShort']) && !empty($document['shortProductName'])) {
            $settings['productShort'] = $document['shortProductName'];
        }

        return $settings;
    }

    /**
     * Generate name by settings
     *
     * @param array $settings
     * @param array $document
     *
     * @return string
     */
    protected function generateName($settings, $document)
    {
        // get model, if document has category field
        $model = isset($document['category'])
            // document is model
            ? $document
            // find model by id
            : $this->mongodb->selectCollection($document['model']['$ref'])
                ->findOne(array(
                    '_id' => $document['model']['$id'],
                ));

        // define words array
        $words = array();
        // iterate settings
        foreach ($settings as $field) {
            switch (true) {
                // if text field
                case preg_match('/^text\|\|\|/', $field['field']):
                    $word = preg_replace('/^text\|\|\|/', '', $field['field']);
                    if ($word) {
                        $words[] = $word;
                    }
                    break;
                // if category field
                case preg_match('/^category-/', $field['field']):
                    $category = $this->mongodb->selectCollection($model['category']['$ref'])
                        ->findOne(array(
                            '_id' => $model['category']['$id'],
                        ));
                    $categoryField = preg_replace('/^category-/', '', $field['field']);
                    if (isset($category[$categoryField])) {
                        $words[] = $category[$categoryField];
                    }
                    break;
                // if brand
                case preg_match('/^brand-/', $field['field']):
                    $brand = $this->mongodb->selectCollection($model['brand']['$ref'])
                        ->findOne(array(
                            '_id' => $model['brand']['$id'],
                        ));
                    $brandField = preg_replace('/^brand-/', '', $field['field']);
                    if (isset($brand[$brandField])) {
                        $words[] = $brand[$brandField];
                    }
                    break;
                // if model
                case preg_match('/^model-/', $field['field']):
                    $modelField = preg_replace('/^model-/', '', $field['field']);
                    if (isset($model[$modelField])) {
                        $words[] = $model[$modelField];
                    }
                    break;
                // if product
                case preg_match('/^product-/', $field['field']):
                    $productField = preg_replace('/^product-/', '', $field['field']);
                    if (isset($document[$productField])) {
                        $words[] = $document[$productField];
                    }
                    break;
                // if parameter
                case preg_match('/^[0-9a-f]{24}$/', $field['field']):
                    // get product if model
                    $product = isset($document['model'])
                        ? $document
                        : $this->mongodb->selectCollection('Products')
                            ->findOne(array(
                                'model.$id' => $document['_id'],
                            ));

                    // define empty values array
                    $values = array();
                    // iterate product parameters
                    foreach ($product['parameters'] as $parameter) {
                        // if parameter is selected
                        if ((string) $parameter['parameter'] == $field['field']) {
                            // distinct values names
                            $values = $this->mongodb->selectCollection('ParameterValues')
                                ->distinct('name', array(
                                    'parameter.$id' => $parameter['parameter'],
                                    '_id'           => array(
                                        '$in' => $parameter['values'],
                                    ),
                                ))->toArray();
                            break;
                        }
                    }
                    // if values
                    if ($values) {
                        // add imploded values to words array
                        $words[] = implode(',', $values);
                    }
                    break;
            }
        }

        // return imploded and trimmed words
        return trim(implode(' ', $words));
    }
}