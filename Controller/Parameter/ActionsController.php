<?php

namespace Nitra\ProductBundle\Controller\Parameter;

use Admingenerated\NitraProductBundle\BaseParameterController\ActionsController as BaseActionsController;

class ActionsController extends BaseActionsController
{
    /**
     * Переопределение для удаления по всем товарам
     * @param \Nitra\ProductBundle\Document\Parameter $parameter
     */
    protected function executeObjectDelete($parameter)
    {
        // удаление значений параметра
        $this->removeParameterValues($parameter);

        $dm = $this->getDocumentManager();

        // удаление параметра из товаров
        $this->removeParameterFromProducts($parameter);

        // удаление самого параметра
        $dm->remove($parameter);
        $dm->flush();

        $dm->clear();
    }

    /**
     * Remove parameter values
     * @param \Nitra\ProductBundle\Document\Parameter $parameter
     */
    protected function removeParameterValues($parameter)
    {
        $this->getDocumentManager()->createQueryBuilder('NitraProductBundle:ParameterValue')
            ->remove()->multiple()
            ->field('parameter')->references($parameter)
            ->getQuery()->execute();
    }

    /**
     * Remove parameter with values from products
     * @param \Nitra\ProductBundle\Document\Parameter $parameter
     */
    protected function removeParameterFromProducts($parameter)
    {
        $parameterId = new \MongoId($parameter->getId());

        $qb = $this->getDocumentManager()->createQueryBuilder('NitraProductBundle:Product')
            ->update()->multiple()
            ->field('parameters.parameter')->equals($parameterId);

        $qb->field('parameters')->pull($qb->expr()
            ->field('parameter')->equals($parameterId)
        );

        $qb->getQuery()->execute();
    }
}