<?php

namespace Nitra\ProductBundle\Controller\Vendor;

use Admingenerated\NitraProductBundle\BaseVendorController\ListController as BaseListController;

class ListController extends BaseListController
{
    protected function processSort($query)
    {
        if ($this->getSortColumn()) {
            parent::processSort($query);
        } else {
            $query->sort(array(
                'name'    => 'asc',
                'vehicle' => 'asc',
                'year'    => 'asc',
            ));
        }
    }

    protected function buildQuery()
    {
        $query = parent::buildQuery();

        $ids = $this->getGroupedVendorsIds(parent::buildQuery());
        $query->field('id')->in($ids);

        return $query;
    }

    /**
     * @param \Doctrine\ODM\MongoDB\Query\Builder $query
     */
    protected function getGroupedVendorsIds($query)
    {
        $query->group(array(
            'name'      => 1,
            'vehicle'   => 1,
            'year'      => 1,
        ), array(
            'ids'       => array(),
        ), 'function (vendor, result) { result.ids.push(vendor._id); }');
        $query->limit(100);
        $results = $query->getQuery()->execute();

        $ids = array();
        foreach ($results as $result) {
            $ids[] = end($result['ids']);
        }

        return $ids;
    }
}