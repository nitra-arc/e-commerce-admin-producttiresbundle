<?php

namespace Nitra\ProductBundle\Controller\Vendor;

use Admingenerated\NitraProductBundle\BaseVendorController\ActionsController as BaseActionsController;
use Nitra\ProductBundle\Form\Type\Vendor\LoadImageType;

class ActionsController extends BaseActionsController
{
    /**
     * Get object Nitra\ProductBundle\Document\Vendor with identifier $pk
     *
     * @param mixed $pk
     * @return \Nitra\ProductBundle\Document\Vendor
     */
    protected function getObject($pk)
    {
        $vendor = $this->getDocumentManager()->find('NitraProductBundle:Vendor', $pk);

        if (!$vendor) {
            throw new \InvalidArgumentException("No Nitra\ProductBundle\Document\Vendor found on id: $pk");
        }

        return $vendor;
    }

    /**
     * @param string $pk
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function attemptObjectLoadImage($pk)
    {
        $vendor = $this->getObject($pk);
        $form   = $this->createLoadImageForm($pk, $Vendor);

        if ($this->getRequest()->isXmlHttpRequest()) {
            $form->handleRequest($this->getRequest());
            if ($form->isSubmitted() && $form->isValid()) {
                $this->setVendorImage($vendor);
                $this
                    ->getRequest()->getSession()->getFlashBag()
                    ->add('success', $this->get('translator')->trans("action.object.edit.success", array(), 'Admingenerator'));
            }
        }
        
        return $this->render('NitraProductBundle:VendorActions:loadImage.html.twig', array(
            'title'     => $this->get('translator')->trans('vendor.actions.load_image', array(), 'NitraProductBundle'),
            'form'      => $form->createView(),
            'Vendor'    => $vendor,
        ));
    }

    /**
     * @param string $pk
     * @param \Nitra\ProductBundle\Document\Vendor $vendor
     * @return \Symfony\Component\Form\Form
     */
    protected function createLoadImageForm($pk, $vendor)
    {
        return $this->createForm(new LoadImageType(), $vendor, array(
            'action'    => $this->generateUrl('Nitra_ProductBundle_Vendor_object', array(
                'pk'        => $pk,
                'action'    => 'loadImage',
            )),
            'attr'      => array(
                'id'        => 'vendor_load_image',
            ),
        ));
    }

    /**
     * @param \Nitra\ProductBundle\Document\Vendor $Vendor
     */
    protected function setVendorImage($vendor)
    {
        $vendors = $this->getDocumentManager()->createQueryBuilder('NitraProductBundle:Vendor')
            ->field('name')->equals($vendor->getName())
            ->field('vehicle')->equals($vendor->getVehicle())
            ->field('year')->equals($vendor->getYear())
            ->getQuery()->execute();

        foreach ($vendors as $vendor) {
            $vendor->setImage($vendor->getImage());
        }

        $this->getDocumentManager()->flush();
    }
}