<?php

namespace Nitra\ProductBundle\Controller\Model;

use Admingenerated\NitraProductBundle\BaseModelController\EditController as BaseEditController;
use Nitra\ProductBundle\Form\Type\Model\EditType;

class EditController extends BaseEditController
{
    /**
     * @return \Symfony\Component\EventDispatcher\EventSubscriberInterface[]
     */
    protected function getFormSubscribers()
    {
        return array(
            $this->get('nitra.model.subscriber'),
            $this->get('nitra.parameters.subscriber'),
        );
    }

    /**
     * @return \Symfony\Component\Security\Core\SecurityContextInterface
     */
    protected function getSecurityContext()
    {
        return $this->get('security.context');
    }

    /**
     * Get form type
     * @return \Symfony\Component\Form\FormTypeInterface
     */
    protected function getEditType()
    {
        $type = new EditType($this->getFormSubscribers());
        $type->setSecurityContext($this->getSecurityContext());

        return $type;
    }
}