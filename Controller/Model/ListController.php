<?php

namespace Nitra\ProductBundle\Controller\Model;

use Admingenerated\NitraProductBundle\BaseModelController\ListController as BaseListController;

class ListController extends BaseListController
{
    /**
     * Add filter condition by active
     *
     * @param \Admingenerator\GeneratorBundle\QueryFilter\QueryFilterInterface $queryFilter
     * @param array $filterObject
     */
    protected function processFilterActive($queryFilter, $filterObject)
    {
        // if filter by active is defined
        if (isset($filterObject['active']) && null !== $filterObject['active']) {
            // get disabled models ids
            $disabledModelsIds = $this->getDisabledModels();
            // get field from query
            $field             = $queryFilter->getQuery()->field('_id');
            // if active yes
            if ($filterObject['active']) {
                // select all models where id not in disabled
                $field->notIn($disabledModelsIds);
            } else {
                // select all models where id in disabled
                $field->in($disabledModelsIds);
            }
        }
    }

    /**
     * Get disabled models ids
     *
     * @return array
     */
    protected function getDisabledModels()
    {
        // find all products
        $products = $this->getDocumentManager()->createQueryBuilder('NitraProductBundle:Product')
            ->hydrate(false)->select('model.$id', 'isActive')
            ->getQuery()->toArray();

        // define empty models array
        $models = array();
        // iterate products
        foreach ($products as $product) {
            // get model id from product as string
            $modelId = (string) $product['model']['$id'];
            // if model id not exists in models array
            if (!array_key_exists($modelId, $models)) {
                // add
                $models[$modelId] = array();
            }
            // add product active flag to models array by model id
            $models[$modelId][] = $product['isActive'];
        }

        // define results array
        $result = array();
        // iterate models
        foreach ($models as $id => $model) {
            // if model not contains active product
            if (!in_array(true, $model) || !$model) {
                // add model id to results array
                $result[] = $id;
            }
        }

        return $result;
    }
}