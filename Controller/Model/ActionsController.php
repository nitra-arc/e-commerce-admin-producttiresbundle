<?php

namespace Nitra\ProductBundle\Controller\Model;

use Admingenerated\NitraProductBundle\BaseModelController\ActionsController as BaseActionsController;
use Symfony\Component\HttpFoundation\RedirectResponse;

class ActionsController extends BaseActionsController
{
    protected function attemptObjectProducts($id)
    {
        $model = $this->getObject($id);

        return $this->render('NitraProductBundle:ModelActions:productList.html.twig', array(
            'model' => $model,
        ));
    }

    /**
     * Process merge models
     *
     * @param array $ids Selected models ids
     *
     * @return RedirectResponse
     */
    protected function attemptBatchMerge($ids)
    {
        if (count($ids) < 2) {
            $this->addFlashMessage('error', 'actions.merge.amountIsLess');
            return new RedirectResponse($this->generateUrl('Nitra_ProductBundle_Model_list'));
        }

        $models = $this->getModelsByIds($ids);

        if ($models->count() < 2) {
            $this->addFlashMessage('error', 'actions.merge.amountIsLess');
            return new RedirectResponse($this->generateUrl('Nitra_ProductBundle_Model_list'));
        }

        $modelsToRemove = array();
        $firstModel     = null;
        foreach ($models as $model) {
            if (is_null($firstModel)) {
                $firstModel = $model;
                continue;
            }

            foreach ($model->getProducts() as $product) {
                $model->removeProduct($product);
                $firstModel->addProduct($product);
            }

            $modelsToRemove[] = $model->getId();
        }

        $this->getDocumentManager()->flush();

        $this->getDocumentManager()->createQueryBuilder('NitraProductBundle:Model')
            ->remove()
            ->field('id')->in($modelsToRemove)
            ->getQuery()->execute();

        $this->addFlashMessage('success', 'actions.merge.successfully');
        return new RedirectResponse($this->generateUrl('Nitra_ProductBundle_Model_list'));
    }

    /**
     * Find models by ids
     *
     * @param array $ids
     *
     * @return \Nitra\ProductBundle\Document\Model[]
     */
    protected function getModelsByIds($ids)
    {
        return $this->getDocumentManager()->createQueryBuilder('NitraProductBundle:Model')
            ->field('id')->in($ids)
            ->getQuery()->execute();
    }

    /**
     * Add flash message
     *
     * @param string $type
     * @param string $text
     */
    protected function addFlashMessage($type, $text)
    {
        $this->get('session')->getFlashBag()->add(
            $type,
            $this->get('translator')->trans(
                $text,
                array(),
                'NitraProductBundleModel'
            )
        );
    }
}