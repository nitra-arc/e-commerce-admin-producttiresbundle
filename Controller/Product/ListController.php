<?php

namespace Nitra\ProductBundle\Controller\Product;

use Admingenerated\NitraProductBundle\BaseProductController\ListController as BaseListController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class ListController extends BaseListController
{
    /**
     * @param \Doctrine\MongoDB\Query\Builder $query
     */
    protected function processFilters($query)
    {
        parent::processFilters($query);

        $filterObject = $this->getFilters();

        $qArray = $query->getQueryArray();
        if (key_exists('status', $qArray)) {
            unset($qArray['status']);
        }
        $query->setQueryArray($qArray);

        if (isset($filterObject['status']) && $filterObject['status']) {
            $query->field('status')->equals(new \MongoRegex('/deleted|.*/'));
        }
    }

    /**
     * Функция для поиска неподвязанных аксессуаров по категории
     * @Route("/find-unselected_accessories", name="Nitra_ProductBundle_findUnselectedAccessoriesByCategory")
     */
    public function findUnselectedAccessoriesByCategory(Request $request)
    {
        $dm = $this->getDocumentManager();
        $category = $request->query->get('category');
        $selected = $request->query->get('products');

        $models   = $dm->createQueryBuilder('NitraProductBundle:Model')
            ->hydrate(false)->select('id')
            ->field('category.id')->equals($category)
            ->getQuery()->execute()->toArray();

        $qb       = $dm
            ->createQueryBuilder('NitraProductBundle:Product')
            ->field('model.id')->in(array_keys($models));

        if (!empty($selected)) {
            $qb->field('id')->notIn(array_values($selected));
        }

        $products = $qb->getQuery()->execute();

        $response = array();
        foreach ($products as $value) {
            $response[$value->getId()] = $value->getModel() . ' ' . $value->getName();
        }

        return new Response(json_encode($response));
    }
}