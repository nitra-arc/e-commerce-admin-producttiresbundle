<?php

namespace Nitra\ProductBundle\Controller\Product;

use Admingenerated\NitraProductBundle\BaseProductController\ActionsController as BaseActionsController;

class ActionsController extends BaseActionsController
{
    use \Nitra\ProductBundle\Traits\ProductActions;
}