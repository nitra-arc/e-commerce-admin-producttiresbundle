<?php

namespace Nitra\ProductBundle\Controller\Product;

use Admingenerated\NitraProductBundle\BaseProductController\EditController as BaseEditController;
use Nitra\ProductBundle\Form\Type\Product\EditType;

class EditController extends BaseEditController
{
    /**
     * @return \Symfony\Component\EventDispatcher\EventSubscriberInterface[]
     */
    protected function getFormSubscribers()
    {
        return array(
            $this->get('nitra.parameters.subscriber'),
            $this->get('nitra.product.subscriber')->setIsSimple(true),
        );
    }

    /**
     * @return \Symfony\Component\Security\Core\SecurityContextInterface
     */
    protected function getSecurityContext()
    {
        return $this->get('security.context');
    }

    /**
     * @return \Nitra\ProductBundle\Form\Type\Product\EditType
     */
    protected function getEditType()
    {
        $type = new EditType($this->getFormSubscribers());
        $type->setSecurityContext($this->get('security.context'));

        return $type;
    }
}