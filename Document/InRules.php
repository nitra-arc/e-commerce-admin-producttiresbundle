<?php

namespace Nitra\ProductBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * @ODM\Document
 */
class InRules
{
    /**
     * @ODM\Id
     */
    protected $id;

    /**
     * @ODM\ReferenceOne(targetDocument="Nitra\ProductBundle\Document\Category")
     */
    protected $category;

    /**
     * @ODM\ReferenceOne(targetDocument="Nitra\ProductBundle\Document\Brand")
     */
    protected $brand;

    /**
     * Get id
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set category
     * @param \Nitra\MainBundle\Document\Category $category
     * @return self
     */
    public function setCategory($category)
    {
        $this->category = $category;
        return $this;
    }

    /**
     * Get category
     * @return \Nitra\MainBundle\Document\Category $category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Clear category
     * @return self
     */
    public function clearCategory()
    {
        $this->category = null;
        return $this;
    }

    /**
     * Set brand
     * @param \Nitra\MainBundle\Document\Brand $brand
     * @return self
     */
    public function setBrand($brand)
    {
        $this->brand = $brand;
        return $this;
    }

    /**
     * Get brand
     * @return \Nitra\MainBundle\Document\Brand $brand
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * Clear brand
     * @return self
     */
    public function clearBrand()
    {
        $this->brand = null;
        return $this;
    }
}