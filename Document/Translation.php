<?php

namespace Nitra\ProductBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Gedmo\Translatable\Document\Translation as BaseTranslation;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Gedmo\Translatable\Document\Translation
 *
 * @ODM\Document(repositoryClass="Nitra\ProductBundle\Repository\TranslationRepository")
 * @ODM\UniqueIndex(name="lookup_unique_idx", keys={
 *         "locale" = "asc",
 *         "object_class" = "asc",
 *         "foreign_key" = "asc",
 *         "field" = "asc"
 * })
 * @ODM\Index(name="translations_lookup_idx", keys={
 *      "locale" = "asc",
 *      "object_class" = "asc",
 *      "foreign_key" = "asc"
 * })
 */
class Translation extends BaseTranslation
{
    use \Gedmo\Timestampable\Traits\TimestampableDocument;
    use \Gedmo\Blameable\Traits\BlameableDocument;
}