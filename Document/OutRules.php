<?php

namespace Nitra\ProductBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * @ODM\Document
 */
class OutRules
{
    /**
     * @ODM\Id
     */
    protected $id;

    /**
     * @ODM\ReferenceOne(targetDocument="Nitra\ProductBundle\Document\Category")
     */
    protected $category;

    /**
     * @ODM\ReferenceOne(targetDocument="Nitra\ProductBundle\Document\Brand")
     */
    protected $brand;

    /**
     * @ODM\ReferenceOne(targetDocument="Nitra\StoreBundle\Document\Store")
     */
    protected $store;

    /**
     * Get id
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set category
     * @param \Nitra\ProductBundle\Document\Category $category
     * @return self
     */
    public function setCategory($category)
    {
        $this->category = $category;
        return $this;
    }

    /**
     * Get category
     * @return \Nitra\ProductBundle\Document\Category $category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Clear category
     * @return self
     */
    public function clearCategory()
    {
        $this->category = null;
        return $this;
    }

    /**
     * Set brand
     * @param \Nitra\ProductBundle\Document\Brand $brand
     * @return self
     */
    public function setBrand($brand)
    {
        $this->brand = $brand;
        return $this;
    }

    /**
     * Get brand
     * @return \Nitra\ProductBundle\Document\Brand $brand
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * Clear brand
     * @return self
     */
    public function clearBrand()
    {
        $this->brand = null;
        return $this;
    }

    /**
     * Set store
     * @param \Nitra\StoreBundle\Document\Store $store
     * @return self
     */
    public function setStore($store)
    {
        $this->store = $store;
        return $this;
    }

    /**
     * Get store
     * @return \Nitra\StoreBundle\Document\Store $store
     */
    public function getStore()
    {
        return $this->store;
    }
}