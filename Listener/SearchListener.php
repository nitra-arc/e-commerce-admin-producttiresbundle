<?php

namespace Nitra\ProductBundle\Listener;

use Doctrine\ODM\MongoDB\Event\OnFlushEventArgs;
use Doctrine\ODM\MongoDB\Event\PostFlushEventArgs;
use Nitra\ProductBundle\Document\Product;
use Nitra\ProductBundle\Document\Category;
use Nitra\ProductBundle\Document\Brand;
use Nitra\ProductBundle\Document\Model;

class SearchListener
{
    /** @var \Symfony\Component\DependencyInjection\Container */
    protected $container;
    /** @var \Doctrine\ODM\MongoDB\DocumentManager */
    protected $dm;
    protected $config;
    protected $entries;

    /**
     * @param \Symfony\Component\DependencyInjection\Container $container
     */
    public function __construct(\Symfony\Component\DependencyInjection\Container $container, array $config)
    {
        $this->container = $container;
        $this->config    = $config;
    }

    /**
     * Get configuration
     *
     * @return array
     */
    public function getConfiguration()
    {
        return $this->config;
    }

    /**
     * @param \Doctrine\ODM\MongoDB\Event\OnFlushEventArgs $args
     */
    public function onFlush(OnFlushEventArgs $args)
    {
        ini_set('memory_limit', '-1');
        $this->dm      = $args->getDocumentManager();
        $uow           = $this->dm->getUnitOfWork();
        $this->entries = array_merge(
            $uow->getScheduledDocumentUpdates(),
            $uow->getScheduledDocumentInsertions()
        );
    }

    /**
     * @param \Doctrine\ODM\MongoDB\Event\PostFlushEventArgs $args
     */
    public function postFlush(PostFlushEventArgs $args)
    {
        foreach ($this->entries as $entry) {
            $this->generateSearchField($this->dm, $entry);
        }
    }

    /**
     * @param \Doctrine\ODM\MongoDB\DocumentManager $dm
     * @param \Nitra\ProductBundle\Document\Product $doc
     */
    protected function generateSearchField(\Doctrine\ODM\MongoDB\DocumentManager $dm, $doc)
    {
        $products = $this->isInstance($dm, $doc);
        if ($products) {
            $productToCheckChanges = end($products);
            if ($productToCheckChanges->getFullNameForSearch() == $this->generate($productToCheckChanges)) {
                return;
            }

            foreach ($products as $product) {
                $dm->getRepository('NitraProductBundle:Product')->createQueryBuilder()
                    ->field('_id')->equals(new \MongoId($product->getId()))
                    ->update()
                    ->field('fullNameForSearch')->set($this->generate($product))
                    ->getQuery()->execute();
            }
        }
    }

    /**
     * @param Product $doc
     * @return string
     */
    protected function generate($doc)
    {
        $words = array();
        foreach ($this->config['fields'] as $field) {
            if ($val = $this->getValue($doc, $field)) {
                $words[] = $val;
            }
        }

        return trim(implode(' ', $words));
    }

    /**
     * @param \Doctrine\ODM\MongoDB\DocumentManager $dm
     * @param object                                $doc
     *
     * @return \Nitra\ProductBundle\Document\Product[]|boolean
     */
    protected function isInstance($dm, $doc)
    {
        $result = false;
        switch (true) {
            case $doc instanceof Product:
                $result = array($doc);
                break;
            case $doc instanceof Model:
                $result = $this->getProductsBy($dm, array(
                    'model.$id' => $this->toMongoId($doc->getId()),
                ));
                break;
            case $doc instanceof Category:
                $models = $dm->createQueryBuilder('NitraProductBundle:Model')
                    ->distinct('_id')
                    ->field('category.id')->equals($doc->getId())
                    ->getQuery()->execute()
                    ->toArray();
                $result = $this->getProductsBy($dm, array(
                    'model.$id' => array(
                        '$in' => $models
                    ),
                ));
                break;
            case $doc instanceof Brand:
                $models = $dm->createQueryBuilder('NitraProductBundle:Model')
                    ->distinct('_id')
                    ->field('brand.id')->equals($doc->getId())
                    ->getQuery()->execute()
                    ->toArray();
                $result = $this->getProductsBy($dm, array(
                    'model.$id' => array(
                        '$in' => $models
                    ),
                ));
                break;
        }

        return $result;
    }

    /**
     * @param \Doctrine\ODM\MongoDB\DocumentManager $dm
     * @param array                                 $parameters
     *
     * @return array
     */
    protected function getProductsBy(\Doctrine\ODM\MongoDB\DocumentManager $dm, array $parameters)
    {
        $products = $dm->getRepository('NitraProductBundle:Product')
            ->findBy($parameters);

        return is_array($products)
            ? $products
            : $products->toArray();
    }

    /**
     * @param \MongoId|string $id
     * @return \MongoId
     */
    protected function toMongoId($id)
    {
        return ($id instanceof \MongoId) ? $id : new \MongoId($id);
    }

    /**
     * Получение значения из товара
     * @param \Nitra\ProductBundle\Document\Product $product
     * @param string $field
     * @return string
     */
    protected function getValue($product, $field)
    {
        $val = null;
        if (is_array($field)) {
            $obj = $product;
            foreach ($field as $method) {
                if (!is_object($obj)) {
                    continue;
                }
                if (method_exists($obj, $method)) {
                    $obj = $obj->$method();
                } else {
                    $repository = $this->dm->getRepository(get_class($obj));
                    $cm         = $repository->getClassMetadata();
                    if ($cm->hasField($method)) {
                        $obj = $repository->createQueryBuilder()
                            ->field('_id')->equals(new \MongoId($obj->getId()))
                            ->distinct($method)
                            ->getQuery()->getSingleResult();
                    }
                }
            }
            if (is_object($obj) || is_string($obj)) {
                $val = (string) $obj;
            }
        } elseif (method_exists($product, $field)) {
            $val = $product->$field();
        } else {
            $repository = $this->dm->getRepository('NitraProductBundle:Product');
            $cm         = $repository->getClassMetadata();
            
            if ($cm->hasField($field)) {
                
                        $val = $repository->createQueryBuilder()
                            ->field('_id')->equals(new \MongoId($product->getId()))
                            ->distinct($field)
                            ->getQuery()->getSingleResult();
            }
            
        }

        return (string) $val;
    }
}