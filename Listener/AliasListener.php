<?php

namespace Nitra\ProductBundle\Listener;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ODM\MongoDB\Event\OnFlushEventArgs;
use Nitra\ProductBundle\Document\Category;
use Nitra\ProductBundle\Document\Product;
use Nitra\ProductBundle\Document\Model;

class AliasListener
{
    /** @var \Symfony\Component\DependencyInjection\ContainerInterface */
    protected $container;
    /** @var \Doctrine\ODM\MongoDB\UnitOfWork */
    protected $uow;
    /** @var object[] */
    protected $entries;
    /** @var object */
    protected $entry;

    /**
     * Constructor
     * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * On flush doctrine event handler
     * @param OnFlushEventArgs $args
     */
    public function onFlush(OnFlushEventArgs $args)
    {
        $dm        = $args->getDocumentManager();
        $this->uow = $dm->getUnitOfWork();

        $this->entries   = array_merge(
            $this->uow->getScheduledDocumentUpdates(),
            $this->uow->getScheduledDocumentInsertions()
        );

        foreach ($this->entries as $entry) {
            $this->entry = $entry;
            if ($entry instanceof Product) {
                //$entry->setFullUrlAliasRu($this->generateProductAliasRu($entry));
                if ($this->checkNeedleToGenerateFullUrlAlias($entry, $this->uow)) {
                    $entry->setFullUrlAliasEn($this->generateProductAliasEn($entry));
                }
            } elseif ($entry instanceof Category) {
                //$entry->setFullUrlAliasRu($this->generateCategoryAliasRu($entry));
                if ($this->checkNeedleToGenerateFullUrlAlias($entry, $this->uow)) {
                    $entry->setFullUrlAliasEn($this->generateCategoryAliasEn($entry));

                    $this->regenerateChildrens($dm, $entry);
                }
            } elseif ($entry instanceof Model) {
                //$entry->setFullUrlAliasRu($this->generateCategoryAliasRu($entry));
                if ($this->checkNeedleToGenerateFullUrlAlias($entry, $this->uow)) {
                    $this->regenerateChildrens($dm, $entry);
                }
            }
            $meta = $dm->getClassMetadata(get_class($entry));
            $this->computeOrRecomputeChanges($meta, $entry, $this->uow);
        }
    }

    /**
     * Compute or recompute document changes
     *
     * @param \Doctrine\ODM\MongoDB\Mapping\ClassMetadata   $meta
     * @param object                                        $entry
     * @param \Doctrine\ODM\MongoDB\UnitOfWork              $uow
     */
    protected function computeOrRecomputeChanges($meta, $entry, $uow)
    {
        // if document has changes
        if ($uow->getDocumentChangeSet($entry)) {
            // recompute
            $uow->recomputeSingleDocumentChangeSet($meta, $entry);
        } else {
            // compute
            $uow->computeChangeSet($meta, $entry);
        }
    }

    /**
     * Check of need to regenerate full url alias
     * @param object                            $object
     * @param \Doctrine\ODM\MongoDB\UnitOfWork  $uow
     * @return boolean
     */
    protected function checkNeedleToGenerateFullUrlAlias($object, $uow)
    {
        $changeSets = $uow->getDocumentChangeSet($object);

        return array_key_exists('aliasEn', $changeSets) || array_key_exists('fullUrlAliasEn', $changeSets);
    }

    /**
     * Перерегенирация дочерних товаров и категорий
     * @param \Doctrine\ODM\MongoDB\DocumentManager $dm
     * @param Category $category
     */
    protected function regenerateChildrens($dm, $category)
    {
        $uow       = $dm->getUnitOfWork();
        $modelsIds = $category instanceof Category
            ? $dm->createQueryBuilder('NitraProductBundle:Model')
                ->hydrate(false)->select('_id')
                ->field('category.id')->equals($category->getId())
                ->getQuery()->execute()->toArray()
            : array($category->getId() => '');

        $products = $dm->createQueryBuilder('NitraProductBundle:Product')
            ->field('model.id')->in(array_keys($modelsIds))
            ->getQuery()->execute();

        foreach ($products as $product) {
            //$product->setFullUrlAliasRu($this->generateProductAliasRu($product));
            $product->setFullUrlAliasEn($this->generateProductAliasEn($product));

            $meta = $dm->getClassMetadata(get_class($product));
            $this->computeOrRecomputeChanges($meta, $product, $uow);
        }

        $categories = $dm->createQueryBuilder('NitraProductBundle:Category')
            ->field('parent.id')->equals($category->getId())
            ->getQuery()->execute();

        foreach ($categories as $child) {
            //$child->setFullUrlAliasRu($this->generateCategoryAliasRu($child));
            $child->setFullUrlAliasEn($this->generateCategoryAliasEn($child));

            $meta = $dm->getClassMetadata(get_class($child));
            $this->computeOrRecomputeChanges($meta, $child, $uow);

            $this->regenerateChildrens($dm, $child);
        }
    }

    protected function generateProductAliasEn($product)
    {
        $slugs = array();
        $slugs[$product->getId()] = $product->getAliasEn();
        if ($this->entry->getId() == $product->getModel()->getId()) {
            $slugs[$product->getModel()->getId()] = $this->entry->getAliasEn();
            $product->setModel($this->entry);
        } else {
            $slugs[$product->getModel()->getId()] = $product->getModel()->getAliasEn();
        }
        $slugs += $this->getParentsCategoriesEn($product->getModel()->getCategory());

        return implode('/', array_reverse($slugs));
    }

    protected function generateCategoryAliasEn($category)
    {
        $slugs = $this->getParentsCategoriesEn($category);

        return implode('/', array_reverse($slugs));
    }

    protected function generateModelAliasEn($model)
    {
        return $model->getCategory()->getAliasEn() . '/' . $model->getAliasEn();
    }

    protected function generateCategoryAliasRu($category)
    {
        $slugs = $this->getParentsCategoriesRu($category);

        return implode('/', array_reverse($slugs));
    }

    protected function getParentsCategoriesEn($category, $slugs = array())
    {
        $slugs[count($slugs) + 1] = $this->entry->getId() == $category->getId()
            ? $this->entry->getAliasEn()
            : $category->getAliasEn();
        if ($category->getParent()) {
            $slugs += $this->getParentsCategoriesEn($category->getParent(), $slugs);
        }

        return $slugs;
    }

    protected function getParentsCategoriesRu($category, $slugs = array())
    {
        $slugs[count($slugs) + 1] = $this->entry->getId() == $category->getId()
            ? $this->entry->getAliasRu()
            : $category->getAliasRu();
        if ($category->getParent()) {
            $slugs += $this->getParentsCategoriesRu($category->getParent(), $slugs);
        }

        return $slugs;
    }
}