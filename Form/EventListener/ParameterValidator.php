<?php

namespace Nitra\ProductBundle\Form\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormError;

class ParameterValidator implements EventSubscriberInterface
{
    public static function getSubscribedEvents()
    {
        return array(
            FormEvents::SUBMIT  => 'onSubmit',
        );
    }

    /**
     * On submit form event handler
     * @param \Symfony\Component\Form\FormEvent $event
     */
    public function onSubmit(FormEvent $event)
    {
        $parameter = $event->getData();
        $form      = $event->getForm();
        if (!$form->has('parameterValues')) {
            return;
        }
        // if parameter values type is digit, check values
        if ($parameter && ($parameter->getValueType() == 'digit')) {
            $this->validateDigitValues($form);
            $this->validateParameterRanges($form, $parameter);
        }

        $this->validateDoublesValues($form);
    }

    /**
     * Validate values on number if value type is digit
     * @param \Symfony\Component\Form\FormInterface $form
     */
    protected function validateDigitValues($form)
    {
        foreach ($form->get('parameterValues')->getData() as $key => $value) {
            if (!is_numeric($value->getName())) {
                $form->get('parameterValues')->get($key)->get('name')->addError(
                    new FormError('digit_value_must_be_numberic')
                );
            }
        }
    }

    /**
     * Validate values on doublicates
     * @param \Symfony\Component\Form\FormInterface $form
     */
    protected function validateDoublesValues($form)
    {
        // define values array
        $values = array();
        // fille values array
        foreach ($form->get('parameterValues')->getData() as $key => $value) {
            $values[$key] = $value->getName();
        }
        foreach ($form->get('parameterValues')->getData() as $key => $value) {
            // clone values array
            $currentValues = $values;
            // unset value by key
            unset($currentValues[$key]);
            // search value in array
            if (in_array($value->getName(), $currentValues)) {
                // add form error to field
                $form->get('parameterValues')->get($key)->addError(
                    new FormError('parameter_value_exists')
                );
            }
        }
    }

    /**
     * Validate correct fill ranges
     * @param \Symfony\Component\Form\FormInterface     $form
     * @param \Nitra\ProductBundle\Document\Parameter   $parameter
     */
    protected function validateParameterRanges($form, $parameter)
    {
        $ranges = ($parameter->getValueType() == 'digit')
            ? $parameter->getParameterRanges()
            : array();

        $multisort       = $this->getRangesMultisort($ranges);
        array_multisort($multisort, SORT_ASC, $ranges);

        foreach ($ranges as $key => $range) {
            switch ($key) {
                case 0:
                    $this->validateFirstRange($range, $form, $key);
                    break;
                case count($ranges) - 1:
                    $this->validateLastRange($range, $ranges, $form, $key);
                    break;
                default:
                    $this->validateRange($range, $ranges, $form, $key);
                    break;
            }
        }

        $parameter->setParameterRanges($ranges);
    }

    /**
     * Getter for multisort array for sorting ranges
     * @param array $ranges
     * @return array
     */
    protected function getRangesMultisort($ranges)
    {
        $multisort       = array();

        foreach ($ranges as $key => $parameterRange) {
            if (is_null($parameterRange['from']) && is_null($parameterRange['to'])) {
                unset($ranges[$key]);
                continue;
            }
            if (is_null($parameterRange['sortOrder'])) {
                $ranges[$key]['sortOrder'] = $key;
            }
            $multisort[$key] = $ranges[$key]['sortOrder'];
        }

        return $multisort;
    }

    /**
     * Validate first range
     * @param array                                 $range
     * @param \Symfony\Component\Form\FormInterface $form
     * @param int                                   $key
     */
    protected function validateFirstRange($range, $form, $key)
    {
        if (is_null($range['to'])) {
            $this->addParameterRangesFormError($form, $key, 'firstRangeToEmpty');
        }
        if (!is_null($range['from'])) {
            $this->addParameterRangesFormError($form, $key, 'firstRangeFromNotEmpty');
        }
    }

    /**
     * Validate last range
     * @param array                                 $range
     * @param array                                 $ranges
     * @param \Symfony\Component\Form\FormInterface $form
     * @param int                                   $key
     */
    protected function validateLastRange($range, $ranges, $form, $key)
    {
        if (!is_null($range['to'])) {
            $this->addParameterRangesFormError($form, $key, 'lastRangeToNotEmpty');
        }
        if (is_null($range['from'])) {
            $this->addParameterRangesFormError($form, $key, 'lastRangeFromEmpty');
        }
        if ($range['from'] < $ranges[$key - 1]['to']) {
            $this->addParameterRangesFormError($form, $key, 'lastRangeFromLessPenultTo');
        }
    }

    /**
     * Validate range
     * @param array                                 $range
     * @param array                                 $ranges
     * @param \Symfony\Component\Form\FormInterface $form
     * @param int                                   $key
     */
    protected function validateRange($range, $ranges, $form, $key)
    {
        if (is_null($range['to'])) {
            $this->addParameterRangesFormError($form, $key, 'rangeFromEmpty', $range['from']);
        }
        if (is_null($range['from'])) {
            $this->addParameterRangesFormError($form, $key, 'rangeToEmpty', null, $range['to']);
        }
        if ($range['from'] > $range['to']) {
            $this->addParameterRangesFormError($form, $key, 'rangeToNotMoreFrom', $range['from'], $range['to']);
        }
        if ($range['from'] < $ranges[$key - 1]['to']) {
            $this->addParameterRangesFormError($form, $key, 'rangeFromLessPenultTo', $range['from'], $range['to']);
        }
    }

    /**
     * Add error to range
     * @param \Symfony\Component\Form\Form  $form
     * @param int                           $key
     * @param string                        $error
     * @param float                         $from
     * @param float                         $to
     */
    protected function addParameterRangesFormError($form, $key, $error, $from = null, $to = null)
    {
        $parameters = array();
        if ($from) {
            $parameters['%rangeFrom%'] = $from;
        }
        if ($to) {
            $parameters['%rangeTo%'] = $to;
        }

        $form->get('parameterRanges')->get($key)->addError(
            new FormError('parameter_ranges.' . $error, null, $parameters)
        );
    }
}