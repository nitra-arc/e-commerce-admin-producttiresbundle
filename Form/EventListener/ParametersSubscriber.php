<?php

namespace Nitra\ProductBundle\Form\EventListener;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormError;
use Nitra\ProductBundle\Document\ParameterValue;

class ParametersSubscriber extends AbstractProductModelParametersSubscriber
{
    protected $parameterValuesSeparator = '==-|-==';

    protected $fnParameterDynamicDigit = 'function (term, data) {
        if ($(data).filter(function () {
            return this.text.localeCompare(term.replace(/,/g, ".")) === 0;
        }).length === 0) {
            var ret = term.replace(/,/g, ".");
            return {
                id:     ret,
                text:   ret
            };
        }
    }';

    protected $fnParameterDynamic = 'function (term, data) {
        if ($(data).filter(function () {
            return this.text.localeCompare(term) === 0;
        }).length === 0) {
            return {
                id:     term,
                text:   term
            };
        }
    }';

    protected $fnParameterDigitMatcher = 'function (term, text) {
        return text.toUpperCase().indexOf(term.replace(/,/g, ".").toUpperCase()) >= 0;
    }';

    /**
     * @param \Symfony\Component\Form\FormEvent $event
     */
    public function preSetData(FormEvent $event)
    {
        $form = $event->getForm();

        if ($form->has('parameters') && $event->getData()) {
            $parameters  = $this->getAllowedParameters($event->getData());

            foreach ($parameters as $id => $parameter) {
                $type = $parameter->getValueType() == 'text'
                    ? 'text'
                    : 'genemu_jqueryselect2_hidden';
                $options = array(
                    'label'              => $parameter->getInternalName(),
                    'mapped'             => false,
                    'data'               => $this->getParameterData($parameter, $event->getData()),
                    'translation_domain' => 'NitraProductBundle',
                    'error_bubbling'     => false,
                    'required'           => $parameter->getIsRequired(),
                );
                if ($type == 'genemu_jqueryselect2_hidden') {
                    $options['configs'] = $this->getParameterConfigs($parameter);
                }

                $form->get('parameters')->add($id, $type, $options);
            }
        }
    }

    /**
     * Get configs for parameter select2 form
     *
     * @param \Nitra\ProductBundle\Document\Parameter $parameter
     *
     * @return array
     */
    protected function getParameterConfigs($parameter)
    {
        $configs = array(
            'width'         => '516px',
            'tags'          => $this->getFormattedParameterValues($parameter),
            'placeholder'   => 'select2.placeholder',
        );
        if (!$parameter->getIsMultiple()) {
            $configs['maximumSelectionSize'] = 1;
        }
        if ($parameter->getIsDynamicValues() && ($parameter->getValueType() == 'digit')) {
            $configs['createSearchChoice'] = $this->fnParameterDynamicDigit;
        } elseif ($parameter->getIsDynamicValues()) {
            $configs['createSearchChoice'] = $this->fnParameterDynamic;
        } else {
            $configs['createSearchChoice'] = 'function () {}';
        }
        if ($parameter->getValueType() == 'digit') {
            $configs['matcher'] = $this->fnParameterDigitMatcher;
        }
        $configs['separator'] = $this->parameterValuesSeparator;

        return $configs;
    }

    /**
     * @param \Nitra\ProductBundle\Document\Parameter $parameter
     * @param \Nitra\ProductBundle\Document\Model|\Nitra\ProductBundle\Document\Product $object
     *
     * @return string
     */
    protected function getParameterData($parameter, $object)
    {
        $product = $this->getProductForParameter($object, $parameter->getId());

        if (!$product) {
            return null;
        }

        $data = array();

        $isText = $parameter->getValueType() == 'text';

        foreach ($product['parameters'] as $productParameter) {
            if ((string) $productParameter['parameter'] == $parameter->getId()) {
                if ($isText) {
                    return $this->dm->find('NitraProductBundle:ParameterValue', end($productParameter['values']));
                }
                $data = $productParameter['values'];
                break;
            }
        }

        return implode($this->parameterValuesSeparator, $data);
    }

    /**
     * @param \Nitra\ProductBundle\Document\Model|\Nitra\ProductBundle\Document\Product $object
     * @param string $parameterId
     *
     * @return array|null
     */
    protected function getProductForParameter($object, $parameterId)
    {
        $qb = $this->dm->createQueryBuilder('NitraProductBundle:Product')
            ->hydrate(false)->select('parameters');

        $field = $object instanceof \Nitra\ProductBundle\Document\Model
            ? 'model.id'
            : 'id';

        $qb->field($field)->equals($object->getId())
            ->field('parameters')->elemMatch($qb->expr()
                ->field('parameter')->equals(new \MongoId($parameterId))
            );

        return $qb->getQuery()->execute()->getSingleResult();
    }

    /**
     * Get allowed parameters for form by model or product
     *
     * @param \Nitra\ProductBundle\Document\Model|\Nitra\ProductBundle\Document\Product $object
     *
     * @return \Nitra\ProductBundle\Document\Parameter[]
     */
    protected function getAllowedParameters($object)
    {
        $type = $object instanceof \Nitra\ProductBundle\Document\Model
            ? 'model'
            : 'product';

        if ($object instanceof \Nitra\ProductBundle\Document\Model) {
            $category = $object->getCategory();
        } elseif (($object instanceof \Nitra\ProductBundle\Document\Product) && $object->getModel()) {
            $category = $object->getModel()->getCategory();
        } else {
            return array();
        }

        $parameters = $this->dm->createQueryBuilder('NitraProductBundle:Parameter')
            ->field('type')->equals($type)
            ->field('categories.$id')->in($this->getCategoriesTreeForParameters($category))
            ->sort('name', 'asc')
            ->getQuery()->execute();

        return $parameters;
    }

    /**
     * @param \Nitra\ProductBundle\Document\Category $category
     *
     * @return \MongoId[]
     */
    protected function getCategoriesTreeForParameters($category)
    {
        $ids = $this->dm->getRepository('NitraProductBundle:Category')
            ->getParentTreeId($category);

        $result = array();
        foreach ($ids as $id) {
            $result[] = new \MongoId($id);
        }

        return $result;
    }

    /**
     * Get and format parameter values for form
     *
     * @param \Nitra\ProductBundle\Document\Parameter $parameter
     *
     * @return array
     */
    protected function getFormattedParameterValues($parameter)
    {
        $values = $this->dm->createQueryBuilder('NitraProductBundle:ParameterValue')
            ->hydrate(false)
            ->select('name')
            ->field('parameter.id')->equals($parameter->getId())
            ->getQuery()
            ->execute()
            ->toArray();

        $result = array();
        foreach ($values as $id => $value) {
            $result[] = array(
                'id'   => $id,
                'text' => $value['name'],
            );
        }

        return $result;
    }

    /**
     * Post submit form handler
     *
     * @param \Symfony\Component\Form\FormEvent $event
     */
    public function postSubmit(FormEvent $event)
    {
        $data = $this->getPostDataFromRequest($event->getForm());

        if (!array_key_exists('parameters', $data)) {
            return;
        }

        $this->validateRequiredParameters($event);

        if (!$event->getForm()->isValid()) {
            return;
        }

        $parameters = $this->createNonExistsParametersValues($data['parameters']);

        $this->removeParameters($parameters, $event->getData());

        $this->insertParameters($parameters, $event->getData());
    }

    /**
     * Create dynamic parameter values
     *
     * @param array $parameters
     *
     * @return array
     */
    protected function createNonExistsParametersValues($parameters)
    {
        foreach ($parameters as $parameterId => &$values) {
            $values = explode($this->parameterValuesSeparator, $values);
            foreach ($values as &$value) {
                if (trim($value) && !preg_match('/^[0-9a-f]{24}$/', $value)) {
                    $value = $this->createNewParameterValue($parameterId, $value);
                }
            }
        }

        return $parameters;
    }

    /**
     * Validate required parameters
     *
     * @param FormEvent $event
     */
    protected function validateRequiredParameters(FormEvent $event)
    {
        foreach ($event->getForm()->get('parameters') as $id => $data) {
            $parameter = $this->dm->find('NitraProductBundle:Parameter', $id);
            if ($parameter->getIsRequired() && !$data->getData()) {
                $event->getForm()->get('parameters')->get($id)->addError(new FormError('This value should not be blank.'));
            }
        }
    }

    /**
     * Create new parameter value from form
     *
     * @param string $parameterId
     * @param string $valueName
     *
     * @return string
     */
    protected function createNewParameterValue($parameterId, $valueName)
    {
        $parameter = $this->dm->find('NitraProductBundle:Parameter', $parameterId);

        $value = $this->dm->createQueryBuilder('NitraProductBundle:ParameterValue')
            ->field('parameter.id')->equals($parameterId)
            ->field('name')->equals($valueName)
            ->getQuery()->execute()
            ->getSingleResult()
            ?
            : new ParameterValue();

        $value->setParameter($parameter);
        $value->setName($valueName);

        $this->dm->persist($value);
        $this->dm->flush($value);

        return $value->getId();
    }

    /**
     * @param array $parameters
     * @param \Nitra\ProductBundle\Document\Model|\Nitra\ProductBundle\Document\Product $object
     */
    protected function removeParameters($parameters, $object)
    {
        $qb = $this->dm->createQueryBuilder('NitraProductBundle:Product')
            ->update()->multiple();

        $field = $object instanceof \Nitra\ProductBundle\Document\Model
            ? 'model.id'
            : 'id';

        $qb->field($field)->equals($object->getId());

        $ids = array();
        foreach (array_keys($parameters) as $id) {
            $ids[] = new \MongoId($id);
        }

        $qb->field('parameters')->pull($qb->expr()
            ->field('parameter')->in($ids)
        );

        $qb->getQuery()->execute();
    }

    /**
     * @param array $parameters
     * @param \Nitra\ProductBundle\Document\Model|\Nitra\ProductBundle\Document\Product $object
     */
    protected function insertParameters($parameters, $object)
    {
        $qb = $this->dm->createQueryBuilder('NitraProductBundle:Product')
            ->update()->multiple();

        $field = $object instanceof \Nitra\ProductBundle\Document\Model
            ? 'model.id'
            : 'id';

        $qb->field($field)->equals($object->getId());

        $insertParameters = array();
        foreach ($parameters as $parameterId => $valuesIds) {
            $insertParameter = $this->createInsertedParameter($parameterId, $valuesIds);
            if (!$insertParameter) {
                continue;
            }
            $insertParameters[] = $insertParameter;
        }

        $qb->field('parameters')->push(array(
            '$each' => $insertParameters,
        ));

        $qb->getQuery()->execute();
    }

    /**
     * @param string $parameterId
     * @param array $valuesIds
     *
     * @return array
     */
    protected function createInsertedParameter($parameterId, $valuesIds)
    {
        $mongodbValuesIds = array();
        foreach ($valuesIds as $id) {
            if (!trim($id)) {
                continue;
            }
            $mongodbValuesIds[] = new \MongoId($id);
        }
        if (!$mongodbValuesIds) {
            return;
        }

        return array(
            '_id'       => new \MongoId(),
            'parameter' => new \MongoId($parameterId),
            'values'    => $mongodbValuesIds,
            'createdAt' => new \MongoDate(),
            'updatedAt' => new \MongoDate(),
        );
    }
}