<?php

namespace Nitra\ProductBundle\Form\EventListener;

use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;

class TranslatableSubscriber implements EventSubscriberInterface
{
    /** @var \Symfony\Component\DependencyInjection\Container */
    protected $container;
    /** @var \Doctrine\ODM\MongoDB\DocumentManager */
    protected $dm;
    /** @var string default locale */
    protected $defaultLocale;

    /**
     * @param \Symfony\Component\DependencyInjection\Container $container
     */
    public function __construct(Container $container)
    {
        // save container
        $this->container        = $container;
        // save document manager
        $this->dm               = $container->get('doctrine_mongodb.odm.document_manager');
        // get and save default locale
        $this->defaultLocale    = $this->container->getParameter('locale');
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return array(
            // on submit
            FormEvents::PRE_SUBMIT   => 'submitData',
            // pre set data
            FormEvents::PRE_SET_DATA => 'preSetData',
        );
    }

    /**
     * @param \Symfony\Component\Form\FormEvent $event
     */
    public function submitData(FormEvent $event)
    {
        // getting object from form
        $object = $event->getForm()->getParent()->getData();

        // if form not contains object
        if (!is_object($object)) {
            // get class name from form options
            $class          = $event->getForm()->getParent()->getConfig()->getDataClass();
            // create object
            $object         = new $class();
            // get class meta data
            $meta           = $this->dm->getClassMetadata($class);
            // set id - hak for embedded documents
            $meta->getReflectionProperty($meta->identifier)->setValue($object, new \MongoId());
            // set created object to from
            $event->getForm()->getParent()->setData($object);
        }

        // get tranaslatable repository
        $repository     = $this->dm->getRepository('NitraProductBundle:Translation');
        foreach ($event->getData() as $locale => $fields) {
            foreach ($fields as $name => $value) {
                if ($locale == $this->defaultLocale) {
                    // set translatable locale
                    $object->setTranslatableLocale($locale);
                    // get class meta data
                    $meta = $this->dm->getClassMetadata(get_class($object));
                    // set value to field
                    $meta->getReflectionProperty($name)->setValue($object, $value);
                // else if value
                } elseif ($value) {
                    // save translation
                    $repository->translate($object, $name, $locale, $value);
                // else
                } else {
                    // get class meta data
                    $meta           = $this->dm->getClassMetadata(get_class($object));
                    // getting identifier
                    $id             = $meta->getReflectionProperty($meta->identifier)->getValue($object);
                    // remove translations
                    $repository->createQueryBuilder()
                        ->remove()
                        // by foreignKey - id
                        ->field('foreignKey')->equals($id)
                        // and objectClass - class name
                        ->field('objectClass')->equals($meta->rootDocumentName)
                        // and field
                        ->field('field')->equals($name)
                        // and locale
                        ->field('locale')->equals($locale)
                        // execute query
                        ->getQuery()->execute();
                }
            }
        }
    }

    /**
     * @param \Symfony\Component\Form\FormEvent $event
     */
    public function preSetData(FormEvent $event)
    {
        // getting object from form
        $object = $event->getForm()->getParent()->getData();
        // if form not contains object
        if (!is_object($object)) {
            return;
        }

        $values = array();

        // get tranaslatable repository
        $repository     = $this->dm->getRepository('NitraProductBundle:Translation');
        // find translations
        $translations   = $repository->findTranslations($object);
        foreach ($event->getForm() as $locale => $fields) {
            $values[$locale] = array();
            foreach ($fields as $name => $field) {
                // define value
                $val = null;
                // locale is default
                if (($locale == $this->defaultLocale)) {
                    // getting value from object
                    $val = $this->dm->getClassMetadata(get_class($object))->getReflectionProperty($name)->getValue($object);
                // if translation exists
                } elseif (key_exists($locale, $translations) && key_exists($name, $translations[$locale])) {
                    // getting
                    $val = $translations[$locale][$name];
                }
                $values[$locale][$name] = $val;
            }
        }

        $event->setData($values);

        return;
    }
}