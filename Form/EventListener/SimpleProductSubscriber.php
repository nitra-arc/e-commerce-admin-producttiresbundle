<?php

namespace Nitra\ProductBundle\Form\EventListener;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class SimpleProductSubscriber implements EventSubscriberInterface
{
    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return array(
            FormEvents::POST_SET_DATA   => 'postSetData',
        );
    }

    /**
     * @param \Symfony\Component\Form\FormEvent $event
     */
    public function postSetData(FormEvent $event)
    {
        $product = $event->getData();
        $form    = $event->getForm();

        $form->get('model')->setData((string) $product->getModel());
    }
}