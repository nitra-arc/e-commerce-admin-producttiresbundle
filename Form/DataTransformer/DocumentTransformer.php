<?php

namespace Nitra\ProductBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Doctrine\ODM\MongoDB\DocumentManager;

class DocumentTransformer implements DataTransformerInterface
{
    /** @var DocumentManager */
    protected $dm;
    /** @var string */
    protected $class;
    /** @var boolean */
    protected $multiple;

    /**
     * @param \Doctrine\ODM\MongoDB\DocumentManager $dm
     * @param string                                $class
     * @param boolean                               $multiple
     */
    public function __construct(DocumentManager $dm, $class, $multiple)
    {
        $this->dm       = $dm;
        $this->class    = $class;
        $this->multiple = $multiple;
    }

    /**
     * @param null|string|object $obj
     * @return array|string|null
     */
    public function transform($obj)
    {
        if (null === $obj) {
            return $this->multiple ? array() : null;
        }

        if ($this->multiple) {
            $result = array();
            foreach ($obj as $is) {
                $result[] = $is->getId();
            }
            return $result;
        } else {
            return is_object($obj) ? $obj->getId() : $obj;
        }
    }

    /**
     * Transforms a string|number|array to an object (issue).
     *
     * @param string|number|array $id
     *
     * @return array|null|object
     */
    public function reverseTransform($id)
    {
        if (!$id) {
            return $this->multiple ? array() : null;
        }

        $results = $this->multiple ? array() : null;
        if (is_array($id)) {
            // Получаем все категории отсортированные по уровню
            $parent     = array();
            $categories = $this->dm->createQueryBuilder('NitraProductBundle:Category')
                ->field('id')->in($id)
                ->sort('level', 1)
                ->getQuery()
                ->execute();

            foreach ($categories as $k => $c) {
                // first level, or
                // проверяем на наличие родителя в массиве
                if (($c->getLevel() == 1) || !$parent || ($parent && !$this->inParentCategory($c->getParent(), $parent))) {
                    $parent[]  = $c->getId();
                    $results[] = $c;
                }
            }
        } else {
            $results = $this->dm->find('NitraProductBundle:Category', $id);
        }

        return $results;
    }

    /**
     * @param \Nitra\ProductBundle\Document\Category $category
     * @param array                                  $parent
     *
     * @return boolean
     */
    protected function inParentCategory($category, $parent)
    {
        if (in_array($category->getId(), $parent)) {
            return true;
        }

        if ($category->getLevel() > 1) {
            return $this->inParentCategory($category->getParent(), $parent);
        }

        return false;
    }
}