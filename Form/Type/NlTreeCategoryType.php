<?php

namespace Nitra\ProductBundle\Form\Type;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\Validator\Constraints;
use Nitra\ProductBundle\Form\DataTransformer\DocumentTransformer;

class NlTreeCategoryType extends AbstractCategoricalType
{
    /**
     * @param \Symfony\Component\Form\FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $transformer = new DocumentTransformer($this->dm, $options['class'], $options['configs']['multiple']);
        $builder->addModelTransformer($transformer);
    }

    /**
     * {@inheritdoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        parent::buildView($view, $form, $options);

        $view->vars['configs']['data'] = $this->getCategoriesHierarchyChild($options['class'], $options['property']);
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'class'             => 'NitraProductBundle:Category',
            'multiple'          => false,
            'placeholder'       => '',
            'property'          => 'treeName',
            'allowClear'        => false,
            'error_bubbling'    => false,
        ));

        $resolver->setNormalizers(array(
            'configs'       => function (Options $options, $configs) {
                $configs['multiple']    = $options->has('multiple')     ? $options->get('multiple')     : false;
                $configs['allowClear']  = $options->has('allowClear')   ? $options->get('allowClear')   : false;
                $configs['placeholder'] = $options->has('placeholder')  ? $options->get('placeholder')  : '';
                return $configs;
            },
            'constraints'   => function (Options $options, array $constraints) {
                if ($options->has('required') && $options->get('required')) {
                    $constraints[] = new Constraints\NotBlank();
                }
                return $constraints;
            },
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'nl_tree_category';
    }

    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return 'genemu_jqueryselect2_hidden';
    }
}