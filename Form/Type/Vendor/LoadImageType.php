<?php

namespace Nitra\ProductBundle\Form\Type\Vendor;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class LoadImageType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('image', 'nlupload', array(
            'label'     => 'vendor.fields.image',
            'required'  => false,
            'multiple'  => false,
            'base_path' => 'images/vendors',
        ));
        $builder->add('save', 'submit', array(
            'label' => 'vendor.save',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'translation_domain' => 'NitraProductBundle',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'vendors_load_image';
    }
}