<?php

namespace Nitra\ProductBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\DependencyInjection\ContainerInterface;

abstract class AbstractCategoricalType extends AbstractType
{
    /** @var \Symfony\Component\DependencyInjection\ContainerInterface */
    protected $container;
    /** @var \Doctrine\ODM\MongoDB\DocumentManager */
    protected $dm;

    /**
     * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container    = $container;
        $this->dm           = $container->get('doctrine_mongodb.odm.document_manager');
    }

    /**
     * Получение дерева категорий, с возможностью выбора только конечной
     *
     * @param string $class
     * @param string $property
     *
     * @return array
     */
    protected function getCategoriesHierarchyChild($class, $property = 'name')
    {
        $categories = $this->dm->createQueryBuilder($class)
            ->hydrate(false)
            ->select(array(
                'path',
                'level',
                $property == 'treeName' ? 'name' : $property,
            ))
            ->sort('level', 1)
            ->sort($property, 1)
            ->getQuery()
            ->execute();

        $result = array();

        foreach ($categories as $id => $category) {
            $pathWith    = explode('|', $category['path']);
            $pathWithout = array_slice($pathWith, 0, count($pathWith) - 2);

            $to = &$result;
            foreach ($pathWithout as $pathItem) {
                $matches = array();
                preg_match('/([\da-f]{24})/i', $pathItem, $matches);
                $to = &$to[$matches[1]]['children'];
            }
            $to[$id] = array(
                'text'     => $property == 'treeName'
                    ? html_entity_decode(str_repeat('&nbsp;&nbsp;', $category['level'])) . $category['name']
                    : $category['name'],
                'children' => array(),
            );
        }

        return $this->formatTreeForSelect2($result);
    }

    protected function formatTreeForSelect2($tree)
    {
        $result = array();
        foreach ($tree as $id => $item) {
            $category = array(
                'id'   => $id,
                'text' => $item['text'],
            );
            if ($item['children']) {
                $category['children'] = $this->formatTreeForSelect2($item['children']);
            }
            $result[] = $category;
        }

        return $result;
    }
}