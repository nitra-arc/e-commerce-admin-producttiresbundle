<?php

namespace Nitra\ProductBundle\Sluggable\Transliterators;

use Gedmo\Sluggable\Util\Urlizer;

class English extends Urlizer
{
    /** @var \Symfony\Component\DependencyInjection\Container */
    protected static $container = null;
    
    public static $lits = array(
        "а" => "a",
        "б" => "b",
        "в" => "v",
        "г" => "g",
        "д" => "d",
        "е" => "e",
        "ё" => "yo",
        "ж" => "zh",
        "з" => "z",
        "и" => "i",
        "й" => "y",
        "к" => "k",
        "л" => "l",
        "м" => "m",
        "н" => "n",
        "о" => "o",
        "п" => "p",
        "р" => "r",
        "с" => "s",
        "т" => "t",
        "у" => "u",
        "ф" => "f",
        "х" => "h",
        "ц" => "c",
        "ч" => "ch",
        "ш" => "sh",
        "щ" => "sh",
        "ъ" => "",
        "ы" => "y",
        "ь" => "",
        "э" => "e",
        "ю" => "yu",
        "я" => "ya",

        "А" => "A",
        "Б" => "B",
        "В" => "V",
        "Г" => "G",
        "Д" => "D",
        "Е" => "E",
        "Ё" => "Yo",
        "Ж" => "Zh",
        "З" => "Z",
        "И" => "I",
        "Й" => "Y",
        "К" => "K",
        "Л" => "L",
        "М" => "M",
        "Н" => "N",
        "О" => "O",
        "П" => "P",
        "Р" => "R",
        "С" => "S",
        "Т" => "T",
        "У" => "U",
        "Ф" => "F",
        "Х" => "H",
        "Ц" => "C",
        "Ч" => "Ch",
        "Ш" => "Sh",
        "Щ" => "Sh",
        "Ъ" => "",
        "Ы" => "Y",
        "Ь" => "",
        "Э" => "E",
        "Ю" => "Yu",
        "Я" => "Ya",

        "є" => "e",
        "Є" => "E",
        "і" => "i",
        "І" => "I",
        "ї" => "yi",
        "Ї" => "Yi",
    );
    
    /**
     * Uses transliteration tables to convert any kind of utf8 character
     *
     * @param string $text
     * @param string $separator
     * @return string $text
     */
    public static function transliterate($text, $separator = '-')
    {
        $as = array();
        if (self::$container && self::$container->hasParameter('sluggable_symbols_replacer')) {
            $as = self::$container->getParameter('sluggable_symbols_replacer');
        }
        if ($as) {
            $replacemend = array_map(function($r) { return '/' . preg_replace('/([^A-Za-z])/', '\\\$1', $r) . '/'; }, array_keys($as));
            $replaced    = array_values($as);
            $text        = preg_replace($replacemend, $replaced, $text);
        }
        
        $text = strtr($text, self::$lits);
        $text = preg_replace("`\[.*\]`U", "", $text);
        $text = preg_replace('`&(amp;)?#?[a-z0-9]+;`i', $separator, $text);
        $text = htmlentities($text, ENT_COMPAT, 'utf-8');
        $text = preg_replace("`&([a-z])(acute|uml|circ|grave|ring|cedil|slash|tilde|caron|lig|quot|rsquo);`i", "\\1", $text);
        $text = preg_replace(array("`[^a-z0-9]`i", "`[-]+`"), $separator, $text);

        return strtolower(trim($text, $separator));
    }
    
    public static function urlize($text, $separator = '-')
    {
        return $text;
    }
}