<?php

namespace Nitra\ProductBundle\Repository;

use Gedmo\Translatable\Document\Repository\TranslationRepository as BaseTranslationRepository;
use Gedmo\Translatable\Mapping\Event\Adapter\ODM as TranslatableAdapterODM;

class TranslationRepository extends BaseTranslationRepository
{
    /**
     * Current TranslatableListener instance used
     * in EntityManager
     *
     * @var \Gedmo\Translatable\TranslatableListener
     */
    private $listener;

    /**
     * Makes additional translation of $document $field into $locale
     * using $value
     *
     * @param object $document
     * @param string $field
     * @param string $locale
     * @param mixed $value
     * @return TranslationRepository
     */
    public function translate($document, $field, $locale, $value)
    {
        $meta       = $this->dm->getClassMetadata(get_class($document));
        $listener   = $this->getTranslatableListener();
        $config     = $listener->getConfiguration($this->dm, $meta->name);
        if (!isset($config['fields']) || !in_array($field, $config['fields'])) {
            throw new \Gedmo\Exception\InvalidArgumentException("Document: {$meta->name} does not translate field - {$field}");
        }
        $modRecordValue = (!$listener->getPersistDefaultLocaleTranslation() && $locale === $listener->getDefaultLocale())
            || $listener->getTranslatableLocale($document, $meta) === $locale
        ;
        if ($modRecordValue) {
            $meta->getReflectionProperty($field)->setValue($document, $value);
            $this->dm->persist($document);
        } else {
            if (isset($config['translationClass'])) {
                $class = $config['translationClass'];
            } else {
                $ea = new TranslatableAdapterODM();
                $class = $listener->getTranslationClass($ea, $meta->rootDocumentName);
            }
            $foreignKey     = $meta->getReflectionProperty($meta->identifier)->getValue($document);
            $objectClass    = $meta->rootDocumentName;
            $transMeta      = $this->dm->getClassMetadata($class);
            $trans          = $this->findOneBy(compact('locale', 'field', 'objectClass', 'foreignKey'));
            if (!$trans) {
                $trans      = $transMeta->newInstance();
                $transMeta->getReflectionProperty('foreignKey')->setValue($trans, $foreignKey);
                $transMeta->getReflectionProperty('objectClass')->setValue($trans, $objectClass);
                $transMeta->getReflectionProperty('field')->setValue($trans, $field);
                $transMeta->getReflectionProperty('locale')->setValue($trans, $locale);
            }
            $mapping        = $meta->getFieldMapping($field);
            $type           = $this->getType($mapping['type']);
            $transformed    = $type->convertToDatabaseValue($value);
            $transMeta->getReflectionProperty('content')->setValue($trans, $transformed);
            if ($this->dm->getUnitOfWork()->isInIdentityMap($document)) {
                $this->dm->persist($trans);
            } else {
                $oid = spl_object_hash($document);
                $listener->addPendingTranslationInsert($oid, $trans);
            }
        }

        return $this;
    }

    /**
     * Get the currently used TranslatableListener
     *
     * @throws \Gedmo\Exception\RuntimeException - if listener is not found
     * @return \Gedmo\Translatable\TranslatableListener
     */
    protected function getTranslatableListener()
    {
        if (!$this->listener) {
            foreach ($this->dm->getEventManager()->getListeners() as $event => $listeners) {
                foreach ($listeners as $hash => $listener) {
                    if ($listener instanceof \Gedmo\Translatable\TranslatableListener) {
                        $this->listener = $listener;
                        break;
                    }
                }
                if ($this->listener) {
                    break;
                }
            }

            if (is_null($this->listener)) {
                throw new \Gedmo\Exception\RuntimeException('The translation listener could not be found');
            }
        }

        return $this->listener;
    }

    protected function getType($type)
    {
        // due to change in ODM beta 9
        return class_exists('Doctrine\ODM\MongoDB\Types\Type')
            ? \Doctrine\ODM\MongoDB\Types\Type::getType($type)
            : \Doctrine\ODM\MongoDB\Mapping\Types\Type::getType($type);
    }
}